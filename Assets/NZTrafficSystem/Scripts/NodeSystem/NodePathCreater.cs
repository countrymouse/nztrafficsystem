﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class NodePathCreater : MonoBehaviour
{
#if UNITY_EDITOR
    public static int nextNodeId = 1;

    static RoadNode root;

    private void OnEnable()
    {
        SceneView.onSceneGUIDelegate += OnSceneGui;
    }

    private void OnDisable()
    {
        SceneView.onSceneGUIDelegate -= OnSceneGui;
    }

    public static void OnSceneGui(SceneView sceneview)
    {
        if (Event.current.type != EventType.KeyDown)
        {
            return;
        }
        if (Event.current.keyCode == KeyCode.S)
        {
            SelectNode();
        }
        if (Event.current.keyCode == KeyCode.A)
        {
            Insert();
        }
        if (Event.current.keyCode == KeyCode.D)
        {
            ForwardNewNode();
        }
    }

    static RoadNode GetFromSelection()
    {
        var nodes = Selection.GetFiltered<RoadNode>(SelectionMode.TopLevel);
        if (null == nodes || nodes.Length < 1)
        {
            return null;
        }
        return nodes[0];
    }

    static void SelectNode()
    {
        root = GetFromSelection();
    }

    static void ForwardNewNode()
    {
        GameObject obj = new GameObject("RoadNode " + nextNodeId);
        RoadNode node = obj.AddComponent<RoadNode>();
        nextNodeId++;

        if (null == root)
        {
            root = node;
            Selection.activeGameObject = root.gameObject;
            return;
        }
        if (null == root.targetNodes)
        {
            root.targetNodes = new List<RoadNode>();
        }

        root.targetNodes.Add(node);

        node.transform.SetParent(root.transform.parent);

        node.transform.position = root.transform.TransformPoint(Vector3.forward * 0.5f);

        root = node;

        Selection.activeGameObject = root.gameObject;
    }

    static void Insert()
    {
        if (null == root)
        {
            return;
        }

        RoadNode from = root;
        RoadNode target = GetFromSelection();

        if (null == target || target == from)
        {
            return;
        }

        if (null == from.targetNodes)
        {
            from.targetNodes = new List<RoadNode>();
        }

        if (from.targetNodes.Contains(target))
        {
            ForwardNewNode();
            root.transform.position = (target.transform.position - from.transform.position) * 0.5f + from.transform.position;
            from.targetNodes.Remove(target);
            root.targetNodes.Add(target);
        }
        else
        {
            from.targetNodes.Add(target);
            root = target;
        }
    }


    private void OnDrawGizmos()
    {
        if (null != root)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(root.transform.position, Vector3.one * 2.0f);
        }
    }
#endif
}
