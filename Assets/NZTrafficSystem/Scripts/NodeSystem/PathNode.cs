﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNode<NODE_TYPE> : MonoBehaviour where NODE_TYPE : MonoBehaviour
{
    public List<NODE_TYPE> targetNodes = new List<NODE_TYPE>();

    protected virtual void OnEnable()
    {
        targetNodes.RemoveAll(x => null == x);
    }

    #region Gizmos
    protected virtual void DrawNodeGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawCube(transform.position, Vector3.one);
    }

    protected virtual void DrawPathGizmos(NODE_TYPE targetNode)
    {
        if (null == targetNode)
        {
            return;
        }
        Gizmos.color = Color.white;
        Gizmos.DrawLine(transform.position, targetNode.transform.position);

        Vector3 pathDir = targetNode.transform.position - transform.position;
        pathDir = pathDir.normalized;
        for(int i = 0; i < 5; i++)
        {
            Gizmos.DrawWireCube(transform.position + pathDir * 0.6f * i, Vector3.one * (0.6f - 0.1f * i));
        }
    }

    protected virtual void OnDrawGizmos()
    {
        DrawNodeGizmos();
        if (null != targetNodes)
        {
            foreach (NODE_TYPE node in targetNodes)
            {
                DrawPathGizmos(node);
            }
        }
    }
    #endregion Gizmos
}
