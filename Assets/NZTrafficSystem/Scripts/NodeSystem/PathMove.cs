﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathMove<PATH_NODE_TYPE> : MonoBehaviour where PATH_NODE_TYPE : MonoBehaviour
{
    public List<PATH_NODE_TYPE> wayPoints = new List<PATH_NODE_TYPE>();

    public bool loop = false;

    [Range (0.0f, 50.0f)]
    public float speed = 10.0f;

    protected int targetWayPointId = 0;

    public PATH_NODE_TYPE recentNode { get; protected set; }

    public virtual PATH_NODE_TYPE targetWayPoint
    {
        get
        {
            try
            {
                return wayPoints[targetWayPointId];
            }
            catch
            {
                return null;
            }
        }
    }

    public virtual void Reset()
    {
        targetWayPointId = 0;
    }

    protected virtual void OnTargetNodeArrive(PATH_NODE_TYPE nodeArrived) { }
    protected virtual void OnSetupNewTargetNode(PATH_NODE_TYPE newTargetNode) { }

    protected virtual void MoveToNext()
    {
        targetWayPointId = loop ? (targetWayPointId + 1) % wayPoints.Count : targetWayPointId + 1;
    }

    protected virtual void MoveToTargetNode()
    {
        if (null == targetWayPoint)
        {
            return;
        }

        float step = Time.deltaTime * speed * GameSpeed.scale;
        Vector3 from = transform.position;
        Vector3 to = targetWayPoint.transform.position;
        float dist = Vector3.Distance(from, to);

        if (dist < step)
        {
            transform.position = to;
            OnTargetNodeArrive(targetWayPoint);
            recentNode = targetWayPoint;
            MoveToNext();
            OnSetupNewTargetNode(targetWayPoint);
        }

        transform.position = Vector3.MoveTowards(from, to, step);

        // only turn to target after move
        if (0.01f < speed)
        {
            transform.LookAt(to);
        }
        
    }

    protected virtual void Update()
    {
        MoveToTargetNode();
    }
}
