﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class PathMoveTester : PathMove<RoadNode>
{
    protected override void OnSetupNewTargetNode(RoadNode newTargetNode)
    {
        if (null == newTargetNode)
        {
            return;
        }
        base.OnSetupNewTargetNode(newTargetNode);
        Debug.Log("Move to " + newTargetNode.gameObject.name);
    }

    protected override void OnTargetNodeArrive(RoadNode nodeArrived)
    {
        if (null == nodeArrived)
        {
            return;
        }
        base.OnSetupNewTargetNode(nodeArrived);
        Debug.Log("Arrive " + nodeArrived.gameObject.name);
    }
}
