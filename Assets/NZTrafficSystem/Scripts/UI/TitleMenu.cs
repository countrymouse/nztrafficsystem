﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleMenu : MonoBehaviour
{
    [SerializeField]
    Button playButton;

    [SerializeField]
    Button exitButton;

    [SerializeField]
    GameObject nextMenu;

    private void Start()
    {
        if (null != playButton)
        {
            playButton.onClick.AddListener(OnPlayButton);
        }
        if (null != exitButton)
        {
            exitButton.onClick.AddListener(Application.Quit);
        }
    }

    void OnPlayButton()
    {
        if (null != nextMenu)
        {
            nextMenu.SetActive(true);
        }
        GameSpeed.gameStart = true;
        gameObject.SetActive(false);
    }
}
