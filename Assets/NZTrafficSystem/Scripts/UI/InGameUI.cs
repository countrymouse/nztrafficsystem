﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{
    [SerializeField]
    Button home;

    [SerializeField]
    GameObject nextMenu;

    [SerializeField]
    Button tutorialExitButton;

    [SerializeField]
    GameObject tutorialPanel;

    [SerializeField]
    GameObject speedPanel;

    private void Start()
    {
        if (null != home)
        {
            home.onClick.AddListener(BackToMainTitle);
        }

        if (null != tutorialExitButton)
        {
            tutorialExitButton.onClick.AddListener(CloseTutorial);
            tutorialExitButton.onClick.AddListener(ShowSpeedControlPanel);
        }
    }

    void ShowSpeedControlPanel()
    {
        if (null != speedPanel)
        {
            speedPanel.SetActive(true);
        }
    }

    private void OnEnable()
    {
        if (null != tutorialPanel)
        {
            tutorialPanel.SetActive(true);
        }
        if (null != speedPanel)
        {
            speedPanel.SetActive(false);
        }
        GameSpeed.scale = 1.0f;
    }

    void CloseTutorial()
    {
        if (null != tutorialPanel)
        {
            tutorialPanel.SetActive(false);
        }
    }

    void BackToMainTitle()
    {
        gameObject.SetActive(false);
        if (null != nextMenu)
        {
            nextMenu.SetActive(true);
        }
        GameSpeed.gameStart = false;
        GameSpeed.scale = 1.0f;
    }
}
