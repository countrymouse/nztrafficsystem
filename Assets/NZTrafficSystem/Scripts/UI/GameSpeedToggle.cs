﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSpeedToggle : MonoBehaviour
{
    public float gameSpeed = 1.0f;
    // Start is called before the first frame update
    void OnEnable()
    {
        var toggle = GetComponent<Toggle>();
        if (null == toggle)
        {
            return;
        }

        toggle.onValueChanged.RemoveAllListeners();
        toggle.onValueChanged.AddListener(OnToggleValueChagned);

        toggle.isOn = 0.1f > Mathf.Abs(gameSpeed - GameSpeed.scale);
    }

    private void OnDisable()
    {
        var toggle = GetComponent<Toggle>();
        if (null == toggle)
        {
            return;
        }
        toggle.isOn = false;
    }

    void OnToggleValueChagned(bool isOn)
    {
        if (isOn)
        {
            GameSpeed.scale = gameSpeed;
        }
    }
}
