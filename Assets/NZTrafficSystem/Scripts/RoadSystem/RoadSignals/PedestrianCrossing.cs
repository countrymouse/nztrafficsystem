﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PedestrianCrossing : RoadArea
{

    protected override void ResetTrackedRoadUserType()
    {
        trackedRoadUserTypeSet = new HashSet<System.Type>();
        trackedRoadUserTypeSet.Add(typeof(Pedestrian));
    }
}
