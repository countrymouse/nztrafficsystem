﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RoadArea : RoadSignalBase
{
    public int roadUserCount { get; protected set; }

    protected HashSet<System.Type> trackedRoadUserTypeSet = null;

    public override bool CanGo()
    {
        return 0 == roadUserCount;
    }

    public override void OnNewUserArrive(RoadUser newRoadUser) { }

    protected virtual void ResetTrackedRoadUserType()
    {
        // null for track all type of components
        trackedRoadUserTypeSet = null;
    }

    protected virtual void OnEnable()
    {
        SetupRigidbody();
        ResetTrackedRoadUserType();
    }

    protected virtual void SetupRigidbody()
    {
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        if (null == rigidbody)
        {
            rigidbody = gameObject.AddComponent<Rigidbody>();
        }
        rigidbody.useGravity = false;
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (IsTrackedRoadUser(other.gameObject))
        {
            ++roadUserCount;
        }
    }

    protected virtual void OnTriggerExit(Collider other)
    {
        if (IsTrackedRoadUser(other.gameObject))
        {
            --roadUserCount;
            roadUserCount = Mathf.Max(0, roadUserCount);
        }
    }

    protected virtual bool IsTrackedRoadUser(GameObject roadUserGameObject)
    {
        if (null == trackedRoadUserTypeSet)
        {
            return false;
        }

        foreach (System.Type roadUserType in trackedRoadUserTypeSet)
        {
            if (null != roadUserGameObject.GetComponent(roadUserType))
            {
                return true;
            }
        }
        return false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = CanGo() ? Color.green : Color.red;
        foreach (BoxCollider co in GetComponentsInChildren<BoxCollider>())
        {
            Matrix4x4 rotationMatrix = Matrix4x4.TRS(co.transform.position, co.transform.rotation, co.transform.lossyScale);
            Gizmos.matrix = rotationMatrix;
            Gizmos.DrawWireCube(co.center, co.size);
        }
    }
}
