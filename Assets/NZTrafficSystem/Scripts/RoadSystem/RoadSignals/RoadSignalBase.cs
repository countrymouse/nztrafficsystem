﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RoadSignalBase : MonoBehaviour
{
    public virtual bool CanGo()
    {
        return true;
    }
    public virtual void OnNewUserArrive(RoadUser newRoadUser)
    {

    }
}
