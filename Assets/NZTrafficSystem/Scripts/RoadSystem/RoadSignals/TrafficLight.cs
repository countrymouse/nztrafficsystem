﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLight : RoadSignalBase
{
    static float globalTrafficLightTimeControl = 0.6f;

    public float greenTime = 8.0f;

    public float redTime = 32.0f;

    public float delayAtStart = 0.0f;

    public GameObject[] enabledOnGreenLight;

    public GameObject[] enabledOnRedLight;

    float timer;

    bool isGreenLight = false;

    private void OnEnable()
    {
        timer = delayAtStart * globalTrafficLightTimeControl;

        SetEnableForObjects(enabledOnRedLight, !isGreenLight);
        SetEnableForObjects(enabledOnGreenLight, isGreenLight);
    }

    public override bool CanGo()
    {
        return isGreenLight;
    }

    protected virtual void Update()
    {
        if (0.0f < timer)
        {
            timer -= Time.deltaTime  * GameSpeed.scale;
        }

        else
        {
            timer = isGreenLight ? redTime : greenTime;
            timer *= globalTrafficLightTimeControl;
            isGreenLight = !isGreenLight;

            SetEnableForObjects(enabledOnRedLight, !isGreenLight);
            SetEnableForObjects(enabledOnGreenLight, isGreenLight);
        }
    }

    void SetEnableForObjects(GameObject [] objs, bool enable)
    {
        if (null == objs)
        {
            return;
        }

        foreach(GameObject obj in objs)
        {
            if (null == obj)
            {
                continue;
            }
            obj.SetActive(enable);
        }
    }

}
