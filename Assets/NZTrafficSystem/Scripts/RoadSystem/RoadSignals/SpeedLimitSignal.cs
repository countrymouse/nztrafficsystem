﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedLimitSignal : RoadSignalBase
{
    public const float MIN_ROAD_SPEED = 2.0f;
    public const float MAX_ROAD_SPEED = 100.0f;
    [Range(MIN_ROAD_SPEED, MAX_ROAD_SPEED)]
    [SerializeField]
    float speedLimit = 10.0f;

    public float roadSpeedLimit
    {
        get
        {
            return speedLimit;
        }
        set
        {
            speedLimit = Mathf.Clamp(value, MIN_ROAD_SPEED, MAX_ROAD_SPEED);
        }
    }

    public override bool CanGo()
    {
        return true;
    }

    public override void OnNewUserArrive(RoadUser newRoadUser)
    {
        if (null == newRoadUser)
        {
            return;
        }

        newRoadUser.roadSpeedLimit = roadSpeedLimit;
    }
}
