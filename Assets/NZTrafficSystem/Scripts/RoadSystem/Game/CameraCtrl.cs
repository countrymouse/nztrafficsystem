﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCtrl : MonoBehaviour
{
    public float moveSpeed = 30.0f;

    float zoomRate;

    Transform target;
    // Start is called before the first frame update
    void Start()
    {
        GameObject obj = new GameObject("Camera Pilot");
        target = obj.transform;
        target.position = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameSpeed.gameStart)
        {
            return;
        }

        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        Vector3 inputVector = target.transform.TransformDirection(x, 0, y).normalized;
        inputVector *= moveSpeed * Time.deltaTime;
        target.transform.position += inputVector;

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        zoomRate = Mathf.Clamp01(zoomRate - scroll * 0.3f);

        float offsetY = -5.0f + zoomRate * 30.0f;
        float rotateX = 20.0f + zoomRate * 60.0f;
        float fov = 65.0f + zoomRate * 35.0f;

        Camera cam = GetComponent<Camera>();
        if (null != cam)
        {
            cam.fieldOfView = fov;
        }

        transform.rotation = Quaternion.Euler(rotateX, 5.0f, 0.0f);

        Vector3 _velocity = Vector3.one;
        transform.position = Vector3.SmoothDamp(transform.position, 
            target.transform.TransformPoint(Vector3.back * 5.0f) + Vector3.up * offsetY, ref _velocity, 0.2f, moveSpeed * 3.0f);
    }
}
