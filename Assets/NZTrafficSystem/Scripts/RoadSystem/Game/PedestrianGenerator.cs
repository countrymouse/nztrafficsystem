﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PedestrianGenerator : Generator<Pedestrian>
{
    protected override void SetupInst(Pedestrian inst)
    {
        base.SetupInst(inst);
        if (null == inst)
        {
            return;
        }
        RoadNode startPos = targetNodes[Random.Range(0, targetNodes.Count)];
        RoadNode endPos = targetNodes[Random.Range(0, targetNodes.Count)];
        inst.transform.position = startPos.transform.position;
        inst.wayPoints.Add(startPos);
        inst.wayPoints.Add(endPos);
        inst.loop = false;
        inst.destoryWhenArrival = true;
    }
}
