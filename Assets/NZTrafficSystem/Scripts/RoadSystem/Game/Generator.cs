﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator<GAME_OBJECT_TYPE> : MonoBehaviour where GAME_OBJECT_TYPE : MonoBehaviour
{
    [SerializeField]
    protected List<GAME_OBJECT_TYPE> prefabs = new List<GAME_OBJECT_TYPE>();

    [SerializeField]
    protected List<RoadNode> targetNodes = new List<RoadNode>();

    [Range(0.5f, 5.0f)]
    public float generateRate = 1.0f;

    [Range(3.0f, 60.0f)]
    public float minGenerateInterval = 5.0f;
    [Range(3.0f, 60.0f)]
    public float maxGenerateInterval = 10.0f;

    public bool generating = true;

    protected float timer;

    protected virtual void OnEnable()
    {
        timer = 1.0f;
        targetNodes.RemoveAll(x => null == x);
        prefabs.RemoveAll(x => null == x);
    }

    protected virtual void Update()
    {
        if (!generating)
        {
            return;
        }

        if (0.0f < timer)
        {
            timer -= Time.deltaTime * generateRate * GameSpeed.scale;
        }
        else
        {
            timer = Random.Range(minGenerateInterval, maxGenerateInterval);

            SetupInst(CreateInstance());
        }
    }

    protected GAME_OBJECT_TYPE CreateInstance()
    {
        GAME_OBJECT_TYPE inst = Instantiate<GAME_OBJECT_TYPE>(prefabs[Random.Range(0, prefabs.Count)]);

        return inst;
    }
    protected virtual void SetupInst(GAME_OBJECT_TYPE inst)
    {
        inst.transform.position = transform.position;
    }

}
