﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleGenerator : Generator<Vehicle>
{
    int count = 0;

    [SerializeField]
    int maxCount = 50;

    protected override void SetupInst(Vehicle inst)
    {
        base.SetupInst(inst);
        if (null == inst)
        {
            return;
        }
        RoadNode startPos = targetNodes[Random.Range(0, targetNodes.Count)];
        inst.wayPoints.Add(startPos);
        inst.loop = true;
        ++count;

        generateRate = (1.0f - count / maxCount) * 2.0f + 1.0f;

        if (count >= maxCount)
        {
            generating = false;
        }
    }
}
