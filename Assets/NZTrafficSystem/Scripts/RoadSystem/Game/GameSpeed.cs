﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSpeed : MonoBehaviour
{
    public static bool gameStart = false;
    static float _scale = 1.0f;
    static public float scale
    {
        get
        {
            return _scale;
        }
        set
        {
            _scale = Mathf.Clamp(value, 0.0f, 2.0f);
        }
    }
    /*
    float savedSpeed = 1.0f;
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            scale = 0.5f;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            scale = 1.0f;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            scale = 2.0f;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (0.0f < scale)
            {
                savedSpeed = scale;
                scale = 0.0f;
            }
            else
            {
                scale = savedSpeed;
            }
        }
    }*/
}
