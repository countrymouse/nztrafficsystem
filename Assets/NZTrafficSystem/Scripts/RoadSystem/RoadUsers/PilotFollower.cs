﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PilotFollower : MonoBehaviour
{
    public Pilot pilot;

    public float smoothTime;

    public Vector3 offsetPosForArrivalCheck = Vector3.zero;

    public Vector3 arrivalPointPosition
    {
        get
        {
            return transform.TransformPoint(offsetPosForArrivalCheck);
        }
    }

    public float speed
    {
        get
        {
            return velocity.magnitude;
        }
    }

    public float sqrtSpeed
    {
        get
        {
            return velocity.sqrMagnitude;
        }
    }

    Vector3 _velocity;
    public Vector3 velocity
    {
        get
        {
            return _velocity;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (null == pilot)
        {
            return;
        }
        transform.position = Vector3.SmoothDamp(transform.position, pilot.transform.TransformPoint(-offsetPosForArrivalCheck), ref _velocity, smoothTime, pilot.drivingSpeed * 1.2f * GameSpeed.scale);
        transform.LookAt(pilot.transform);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Vector3 frontPos = transform.TransformPoint(offsetPosForArrivalCheck);
        Gizmos.DrawWireCube(frontPos, Vector3.one * 0.2f);
        if (null != pilot)
        {
            Gizmos.DrawLine(frontPos, pilot.transform.position);
        }
    }
}
