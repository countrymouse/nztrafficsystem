﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pilot : RoadUser
{
    public float maxTrafficSpeed = SpeedLimitSignal.MAX_ROAD_SPEED;

    public float drivingSpeed
    {
        get
        {
            return Mathf.Min(maxOnRoadSpeed, maxTrafficSpeed);
        }
    }

    public PilotFollower follower;

    public override void OnRoadSpeedLimitChanged()
    {
        base.OnRoadSpeedLimitChanged();
        maxTrafficSpeed = roadSpeedLimit;
    }

    protected override float CalculateSpeed()
    {
        return waitForSignal ? 0.0f : drivingSpeed;
    }

    private void OnTriggerStay(Collider other)
    {
        PilotFollower pf = other.gameObject.GetComponent<PilotFollower>();
        if (null == pf || follower == pf)
        {
            return;
        }

        maxTrafficSpeed = pf.speed;
    }

    private void OnTriggerExit(Collider other)
    {
        PilotFollower pf = other.gameObject.GetComponent<PilotFollower>();
        if (null == pf || follower == pf)
        {
            return;
        }
        
        maxTrafficSpeed = maxOnRoadSpeed;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position, Vector3.one);
    }
}
