﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vehicle : MonoBehaviour
{
    [Header("Path")]
    public List<RoadNode> wayPoints = new List<RoadNode>();

    public bool loop = true;


    [Header("Settings")]
    [Range(0.0f, SpeedLimitSignal.MAX_ROAD_SPEED)]
    public float maxSpeed = 10.0f;

    public float smoothFollowTime = 0.2f;

    public Vector3 offsetToFront = Vector3.zero;


    Pilot pilot;

    private void Start()
    {
        TryCreatePilot();
        SetupPilotFollower();
        SetupPilot();
    }

    private void OnDestroy()
    {
        if (null != pilot)
        {
            Destroy(pilot.gameObject);
        }
    }

    private void OnEnable()
    {
        if (null != pilot)
        {
            pilot.gameObject.SetActive(true);
        }
    }

    private void OnDisable()
    {
        if (null != pilot)
        {
            pilot.gameObject.SetActive(false);
        }
    }

    void TryCreatePilot()
    {
        if (null == pilot)
        {
            GameObject obj = new GameObject(gameObject.name + " - Pilot");
            pilot = obj.AddComponent<Pilot>();
        }
    }

    void SetupPilot()
    {
        if (null == pilot)
        {
            return;
        }
        pilot.transform.position = transform.TransformPoint(offsetToFront);

        pilot.follower = GetComponent<PilotFollower>();
        pilot.maxMoveSpeed = maxSpeed;

        pilot.loop = loop;
        wayPoints.RemoveAll(x => null == x);
        pilot.wayPoints = wayPoints;
        

        BoxCollider pilotTrigger = pilot.GetComponent<BoxCollider>();
        if (null == pilotTrigger)
        {
            pilotTrigger = pilot.gameObject.AddComponent<BoxCollider>();
        }
        pilotTrigger.isTrigger = true;
    }

    void SetupPilotFollower()
    {
        PilotFollower follower = GetComponent<PilotFollower>();
        if (null == follower)
        {
            follower = gameObject.AddComponent<PilotFollower>();
        }

        follower.smoothTime = smoothFollowTime;
        follower.offsetPosForArrivalCheck = offsetToFront;
        follower.pilot = pilot;
    }
}
