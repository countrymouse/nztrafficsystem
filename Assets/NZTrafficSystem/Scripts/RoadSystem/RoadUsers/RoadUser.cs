﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadUser : MoveByKeyRoadNode
{
    public float maxMoveSpeed = 20.0f;

    [SerializeField]
    float maxRoadSpeed = SpeedLimitSignal.MAX_ROAD_SPEED;

    public bool destoryWhenArrival = false;

    public float roadSpeedLimit
    {
        get
        {
            return maxRoadSpeed;
        }
        set
        {
            maxRoadSpeed = Mathf.Clamp(value, SpeedLimitSignal.MIN_ROAD_SPEED, SpeedLimitSignal.MAX_ROAD_SPEED);
            OnRoadSpeedLimitChanged();
        }
    }

    public float maxOnRoadSpeed
    {
        get
        {
            return Mathf.Min(roadSpeedLimit, maxMoveSpeed);
        }
    }

    public bool waitForSignal
    {
        get
        {
            if (null == signalList || 0 == signalList.Count)
            {
                return false;
            }

            bool pass = true;
            foreach(RoadSignalBase signal in signalList)
            {
                if (null == signal)
                {
                    continue;
                }
                if (!signal.CanGo())
                {
                    pass = false;
                }
            }

            if (pass)
            {
                signalList = null;
            }

            return !pass;
        }
    }

    List<RoadSignalBase> signalList = null;

    public virtual bool isFullStop
    {
        get
        {
            return 0.0001f > speed;
        }
    }

    public virtual void OnRoadSpeedLimitChanged()
    {

    }

    protected virtual float CalculateSpeed()
    {
        return waitForSignal ? 0.0f : maxOnRoadSpeed;
    }

    protected override void Update()
    {
        speed = CalculateSpeed();
        base.Update();
    }

    protected override void OnSetupNewTargetNode(RoadNode newTargetNode)
    {
        base.OnSetupNewTargetNode(newTargetNode);
        if (!loop && destoryWhenArrival && null == targetWayPoint)
        {
            Destroy(gameObject);
            return;
        }
    }

    protected override void OnTargetNodeArrive(RoadNode nodeArrived)
    {
        base.OnTargetNodeArrive(nodeArrived);

        if (null == nodeArrived)
        {
            return;
        }

        signalList = nodeArrived.GetSignals();
        if (null != signalList)
        {
            foreach(RoadSignalBase signal in signalList)
            {
                if (null == signal)
                {
                    continue;
                }
                signal.OnNewUserArrive(this);
            }
        }
    }
}
