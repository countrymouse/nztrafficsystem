﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoadNode : PathNode<RoadNode>
{
    [SerializeField]
    protected List<RoadSignalBase> signals = new List<RoadSignalBase>();

    protected override void OnEnable()
    {
        base.OnEnable();
        signals.RemoveAll(x => null == x);
    }

    public List<RoadSignalBase> GetSignals()
    {
        if (null == signals)
        {
            signals = new List<RoadSignalBase>();
        }
        return signals.ToList();
    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();
        Gizmos.color = Color.cyan;
        foreach (RoadSignalBase signal in signals)
        {
            if (null != signal)
            {
                Gizmos.DrawLine(signal.transform.position, transform.position);
            }
        }
    }
}
