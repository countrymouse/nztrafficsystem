﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveByKeyRoadNode : PathMove<RoadNode>
{
    RoadNode recentTargetNode = null;

    RoadNode GetRecentTargetNode()
    {
        if (null == recentNode)
        {
            return targetWayPoint;
        }

        if (recentNode.targetNodes.Contains(targetWayPoint))
        {
            return targetWayPoint;
        }

        return recentNode.targetNodes[Random.Range(0, recentNode.targetNodes.Count)];
    }

    protected override void MoveToTargetNode()
    {
        if (null == recentTargetNode)
        {
            recentTargetNode = GetRecentTargetNode();
            if (null == recentTargetNode)
            {
                return;
            }
        }

        float step = Time.deltaTime * speed * GameSpeed.scale;
        Vector3 from = transform.position;
        Vector3 to = recentTargetNode.transform.position;
        float dist = Vector3.Distance(from, to);

        if (dist < step)
        {
            transform.position = to;
            OnTargetNodeArrive(recentTargetNode);

            if (wayPoints.Contains(recentTargetNode))
            {
                MoveToNext();
            }
            recentNode = recentTargetNode;
            recentTargetNode = GetRecentTargetNode();
            OnSetupNewTargetNode(recentTargetNode);
        }

        transform.position = Vector3.MoveTowards(from, to, step);

        // only turn to target after move
        if (0.01f < speed)
        {
            transform.LookAt(to);
        }
    }
}
