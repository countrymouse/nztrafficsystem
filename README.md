
![alt text](https://bitbucket.org/countrymouse/nztrafficsystem/raw/7c0c8a570309dbb2bce1e5a64396b9ea7a36d058/ShowCaseImages/Intro.png "Intro")

**[ Game Title ]** : NZ Traffic System

**[ Game Type ]** : Sim/Demo

**[ Language ]** : English

**[ Platform ]** : PC/WebGL

**[ Size ]** : 24MB (PC) / 8MB (WebGL)


**[Download]** : 

Windows Standalone 24MB

[NZTrafficSystemWin.zip](https://drive.google.com/open?id=1y0mOjWfN7ZwzmxGKR0ryuya8WJswK0kM)

WebGL 8MB

[NZTrafficSystemWebGL.zip](https://drive.google.com/open?id=1cxFfmemnktDW1anzl32fEe9Kou0v6g-U)

Unity  Project & Source Code 24MB

[NZTrafficSystemProject.zip](https://drive.google.com/open?id=1nF0DkB8e8IMIN3DWFvU0K3W_mIj4XlJq)

Online Source Code

[https://bitbucket.org/countrymouse/nztrafficsystem](https://bitbucket.org/countrymouse/nztrafficsystem)

**[Features]**
Give Way to Right

![alt text](https://bitbucket.org/countrymouse/nztrafficsystem/raw/7c0c8a570309dbb2bce1e5a64396b9ea7a36d058/ShowCaseImages/GiveWayRight.gif "Give Way to Right")

Give Way to Pedestrian

![alt text](https://bitbucket.org/countrymouse/nztrafficsystem/raw/7c0c8a570309dbb2bce1e5a64396b9ea7a36d058/ShowCaseImages/GiveWayToPedestrian.gif "Give Way to Pedestrian")

Roundabout

![alt text](https://bitbucket.org/countrymouse/nztrafficsystem/raw/7c0c8a570309dbb2bce1e5a64396b9ea7a36d058/ShowCaseImages/Roundabout.gif "Roundabout")

Traffic Lights

![alt text](https://bitbucket.org/countrymouse/nztrafficsystem/raw/7c0c8a570309dbb2bce1e5a64396b9ea7a36d058/ShowCaseImages/TrafficLights.gif "Traffic Lights")

Tutorial & Speed Control

![alt text](https://bitbucket.org/countrymouse/nztrafficsystem/raw/7c0c8a570309dbb2bce1e5a64396b9ea7a36d058/ShowCaseImages/Controls.png "Tutorial & Speed Control")

